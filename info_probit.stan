
data {
  int<lower=0> n; // number of observations (train data)
  int<lower=0> N; // number of observations (all data)
  int<lower=0> k; // number of predictors; (= beta)
  matrix[n,k] X_train; // design matrix (intercept not included; train data)
  matrix[N,k] X_all; // design matrix (intercept not included; all data)
  int<lower=0,upper=1> Y_train[n]; // response vector (train data)
  
  // Information of train data (no need for non-informative prior)
  real mmean[k]; // mean
  real<lower=0> ssd[k]; // standard deviation
}

parameters {
  vector[k] beta;
  real alpha;
}

model {
  vector[n] eta;
  eta = alpha + X_train*beta;
  
  alpha ~ normal(0, 1);
  beta ~ normal(mmean, ssd);

  Y_train ~ bernoulli(Phi(eta));
}

generated quantities {
  vector[N] y_return;
  for(i in 1:N) {
    y_return[i] = bernoulli_rng(Phi(alpha + X_all[i]*beta));
  }
}
