---
title: "R Notebook"
output: html_notebook
---
```{r}
library(plyr)
library(dplyr)
library(tidyr)
library(corrplot)
library(rstan)
library(bayesplot)
```

```{r}
data <- read.csv("~/Downloads/STATS551/Group_Project/data_with_label.csv", sep = ",", header = T)
head(data)
```

```{r}
data <- data %>% select(-X)
```

```{r}
str(data)
```

```{r}
data
```


```{r}
#for(i in 2:11){
#  data[,i] <- rescale(data[,i], to = c(0,1))
#}

scaled_X <- scale(data[,-c(1,12)])
scaled_data <- data.frame(bookingID=data$bookingID, scaled_X, label=data$label)

#junk$nm[junk$nm == "B"] <- "b"
#scaled_data$label[scaled_data$label == 0] <- 1e-16

scaled_data
```

```{r}
scaled_data$row_sum = rowSums(scaled_data[,-c(1, 12)])
summary(scaled_data$row_sum)
dim(scaled_data)
quantile(scaled_data$row_sum,0.25)
```

```{r}
scaled_data <- scaled_data[(scaled_data$row_sum > quantile(scaled_data$row_sum,0.25)),]
#scaled_data <- scaled_data[(scaled_data$row_sum > 0),]
# Editted part
rownames(scaled_data) <- 1:dim(scaled_data)[1]

dim(scaled_data)
scaled_data
```

## Split the data into 2 sets, train and test (75:25)
```{r}
set.seed(4992)
train_size <- round((dim(scaled_data)[1])*0.75)
train_size

train_index <- sample(1:dim(scaled_data)[1], train_size, replace = FALSE)
length(train_index)
train_data <- scaled_data %>% filter(rownames(scaled_data) %in% train_index)

head(train_data)
```

## Construnct the model by using train data
```{r}
dim(train_data)
```

ingredient
```{r}
n <- dim(train_data)[1]
N <- dim(scaled_data)[1]
k <- dim(scaled_data)[2] - 3
X_train <- as.matrix(train_data[,-c(1,12,13)])
X_all <- as.matrix(scaled_data[,-c(1,12,13)])
Y_train <- as.numeric(as.character(train_data[,12]))

dat <- list(n = n, N = N, k = k, X_train = X_train, X_all = X_all, Y_train = Y_train)
```

```{r}
weakly_info_model <- stan_model("~/Downloads/STATS551/Group_Project/weakly_info_probit.stan")
```

```{r, results = 'hide'}
fit <- sampling(weakly_info_model, dat, iter = 10000, seed = 123, control = list(adapt_delta = 0.8))
```

```{r}
result_1 <- as.data.frame(fit)
```

```{r}
result_1
```

```{r}
mcmc_trace(as.array(fit)[,,1:11], facet_args = list(nrow = 4))
```

```{r}
mcmc_trace(result_1[,1:11], facet_args = list(nrow = 4))
```

```{r}
(data.frame(summary(fit)$summary[1:11, "Rhat"], summary(fit)$summary[1:11, "n_eff"]))
```

```{r}
cred_inv <- function(x){
  return(quantile(x,c(0.025,0.975)))
}
```



```{r}
data.frame(apply(result_1[,1:11],2,mean), apply(result_1[,1:11],2,sd))
```

```{r}
data.frame(t(apply(result_1[,1:11],2,cred_inv)))
```

```{r}
sort(train_index)[1:20]
```


```{r}
train_result <- as.numeric(round(apply(result_1[, sort(train_index + 11)],2,mean)))
```

```{r}
length(train_result)
length(Y_train)
mean(train_result != Y_train)
```

```{r}
test_index <- (1:dim(X_all)[1])[!(1:dim(X_all)[1]) %in% train_index]
```

```{r}
test_result <- as.numeric(round(apply(result_1[, sort(test_index + 11)],2,mean)))
```

```{r}
mean(test_result != data$label[sort(test_index)])
```












